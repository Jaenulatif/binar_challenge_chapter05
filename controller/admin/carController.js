const { Car } = require('../../models')
const imagekit = require('../../lib/imageKit')
const { Op } = require("sequelize")
const axios = require("axios")

const homepage = async (req, res) => {
    res.render('index')
}

const carsPage = async (req, res) => {
    res.render('car')
}

const detailPage = async (req, res) => {
    const id = req.params.id
    const nama = req.params.nama

    const query = req.query.q
    console.log(query)
    const item = awaitCar.findOne({
        where: {
            [Op.or]: [
                { id: id },
                { nama: query }
            ]
        }
    })

    res.render('detail', {
        id,
        nama,
        item,
        query
    })
}

const dataPage = async (req, res) => {
    const nm = req.query.nama
    const items = await Car.findAll()
    res.render('cars', {
        items,
        nm
    })
}

const createPage = async (req, res) => {
    res.render('add')
}

// controller create
const createCar = async (req, res) => {
    const { nama, sewa, ukuran, foto } = req.body
    const split = req.file.originalname.split('.')
    const ext = split[split.length - 1]

    // upload file to imagekit
    const img = await imagekit.upload({
        file: req.file.buffer,
        fileName: `${req.file.originalname}.${ext}`,
    })
    console.log(img.url)

    const newCar = await Car.create({
        nama,
        sewa,
        ukuran,
        foto: img.url,
    })
    res.redirect('/admin/cars')
}

const editPage = async (req, res) => {
    const item = await Car.findByPk(req.params.id)
    res.render('edit', {
        item
    })
}

// controller edit
const editCar = async (req, res) => {
    const { nama, sewa, ukuran } = req.body
    const id = req.params.id
    await Car.update({
        nama,
        sewa,
        ukuran,
    }, {
        where: {
            id
        }
    })
    res.redirect('/admin/cars')
}

// controller delete
const deleteCar = async (req, res) => {
    const id = req.params.id
    await Car.destroy({
        where: {
            id
        }
    })
    res.redirect('/admin/cars')
}

const findCar = async (req, res) => {
    if (req.query.nama && req.query.ukuran == "") {
        const nm = req.query.nama
        const items = await Car.findAll({
            where: {
                nama: { [Op.iLike]: req.query.nama ? `%${req.query.nama}%` : null }
            }
        })
        res.render('cars', {
            items,
            nm
        })
    } else if (req.query.nama == "" && req.query.ukuran) {
        const nm = req.query.nama
        const items = await car.findAll({
            where: {
                ukuran: req.query.ukuran ? req.query.ukuran : null
            }
        })
        res.render('cars', {
            items,
            nm
        })
    } else if (req.query.nama && req.query.ukuran) {
        const nm = req.query.nama
        const items = await Car.findAll({
            where: {
                [Op.and]: [
                    { nama: { [Op.iLike]: req.query.nama ? `%${req.query.nama}%` : null } },
                    { ukuran: req.query.ukuran ? req.query.ukuran : null }
                ]
            }
        })
        res.render('cars', {
            items,
            nm
        })
    } else {
        const nm = ""
        const items = await Car.findAll()
        res.render('cars', {
            items,
            nm
        })
    }
}

module.exports = {
    homepage,
    dataPage,
    createPage,
    createCar,
    editPage,
    editCar,
    deleteCar,
    detailPage,
    carsPage,
    findCar
}