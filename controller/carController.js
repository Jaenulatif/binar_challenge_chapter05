const { Car } = require('../models')
const imagekit = require('../lib/imageKit')
const { Op } = require("sequelize")

const createCar = async(req, res) => {
    const { nama, sewa, ukuran, foto } = req.body
    try {
        // untuk dapat extension file nya
        const split = req.file.originalname.split('.')
        const ext = split[split.length - 1]

        // upload file ke imagekit
        const img = await imagekit.upload({
            file: req.file.buffer, 
            fileName: `${req.file.originalname}.${ext}`, 
        })
        console.log(img.url)

        const newCar = await Car.create({
            nama,
            sewa,
            ukuran,
            foto: img.url,
        })

        res.status(201).json({
            status: 'success',
            data: {
                newCar
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const findCar = async(req, res) => {
    try {
        if(req.query){
            car = await Car.findAll({
                where: {
                    [Op.or]: [
                        {nama: {[Op.like]: req.query.nama ? req.query.nama.charAt(0).toUpperCase() + req.query.nama.slice(1):null }},
                        {ukuran: req.query.ukuran? req.query.ukuran:null}
                    ]
                    
                }
            })
            res.status(200).json({
                status: 'Success',
                data: {
                    car
                }
            })
        } else {
            car = await Car.findAll()
            res.status(200).json({
                status: 'Success',
                data: {
                    car
                }
            })
        }
        
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const editCar = async(req, res) => {
    try {
        const { nama, sewa, ukruan } = req.body
        const id = req.params.id
        const car = await Car.update({
            nama,
            sewa,
            ukuran,
        }, {
            where: {
                id
            }
        })
        res.status(200).json({
            status: 'Success',
            data: {
                id,
                nama,
                sewa,
                ukuran,
            }
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

const deleteCar = async(req, res) => {
    try {
        const id = req.params.id
        await Car.destroy({
            where: {
                id
            }
        })

        res.status(200).json({
            status: 'success',
            message: `Kendaraan dengan id ${id} telah dihapus`
        })
    } catch (err) {
        res.status(400).json({
            status: 'fail',
            errors: [err.message]
        })
    }
}

module.exports = {
    createCar,
    findCar,
    editCar,
    deleteCar,
}