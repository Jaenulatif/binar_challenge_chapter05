require('dotenv').config()

const express = require('express')

const morgan = require('morgan')

const router = require('./routes')

const cors = require('cors')

const port = process.env.PORT

const app = express()

app.locals.moment = require('moment')

app.use(express.json())
app.use(cors())

app.use(express.urlencoded({ extended: false }))

app.set('view engine', 'ejs')

app.use(morgan('dev'))
app.use(router)

app.use(express.static('public'))

app.listen(port, () => {
    console.log(`Server runing in localhost:${port}`)
})