# RESTful API dengan Express, Sequelize, dan ejs

Project ini dibuat menggunakan NodeJS dan dengan memanfaatkan beberapa module seperti Express, Sequelize, ImageKit, dan EJS untuk View Engine

## Install dependency

```bash
# Pengguna NPM
npm install

```

## run sequelize

```
sequelize db:create
sequelize db:migrate
```

## Jalankan server

```
npm start
```

## halaman admin

```
http://localhost:8070/admin
```
