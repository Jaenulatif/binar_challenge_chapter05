const router = require('express').Router()
const Car = require('../controller/carController')
const Admin = require('../controller/admin/carController')
const Upload =  require('../controller/uploadController')

// middleware
const uploader = require('../middlewares/uploader')

// admin client side
router.get('/admin', Admin.homepage)
router.get('/admin/cars', Admin.dataPage)
router.get('/admin/add', Admin.createPage)
router.post('/admin/add', uploader.single('image'), Admin.createCar)
router.get('/admin/edit/:id', Admin.editPage)
router.post('/admin/edit/:id', Admin.editCar)
router.post('/admin/delete/:id', Admin.deleteCar)
router.get('/admin/car/filter', Admin.findCar)

// API server
router.post('/api/car', uploader.single('image'), Car.createCar)
router.get('/api/car', Car.findCar)
router.put('/api/car/:id', Car.editCar)
router.delete('/api/car/:id', Car.deleteCar)

module.exports = router